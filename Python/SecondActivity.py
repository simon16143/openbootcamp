
#@author Symon-Lopez
name = input("What's your name? ")
print(f"Hi, {name}! welcome to the first activity into OpenBootCamp")

#I can modify the name variable because python is a weakly typed language
name = 5
print(name)