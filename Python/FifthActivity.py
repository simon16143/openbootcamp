year = int(input("Insert the year..."))

def verify_year(*wargs):
    if year % 4 == 0 or year % 400 == 0:
        return ("The year inserted is a leap year.")
    else:
        return ("The year inserted is not a leap year.")

print(verify_year(year))    